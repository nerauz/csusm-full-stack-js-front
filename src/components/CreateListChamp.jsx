import React from 'react';
import champions from 'lol-champions';
import CreateChamp from './CreateChamp';
import '../css/Champions.css';

class CreateListChamp extends React.PureComponent {
  render() {
    return (
      <div id="champions">
        { champions.map((item) => <CreateChamp key={item.key} data={item} />) }
      </div>
    );
  }
} export default CreateListChamp;
