import { React, PureComponent } from 'react';
import { Switch, Route } from 'react-router-dom';

import CreateCompte from './CreateCompte';
import Signin from './Signin';
import Home from './Home';
import ChampPage from './ChampPage';
import Chat from './Chat/Chat';
import Join from './Chat/Join';

class Routes extends PureComponent {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/signup" component={CreateCompte} />
        <Route exact path="/signin" component={Signin} />
        <Route path="/chat" component={Chat} />
        <Route path="/join" component={Join} />
        <Route path="/:name" component={ChampPage} />

      </Switch>
    );
  }
} export default Routes;
