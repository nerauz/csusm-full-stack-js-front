import axios from 'axios';
import React, { useState, useEffect } from 'react';

export default function ChampPage({ id }) {
  const [Otps, setOtps] = useState([]);
  useEffect(() => {
    axios.get(`http://localhost:3000/otp?champId=${id}`).then((res) => {
      setOtps(res.data);
    });
  }, []);
  return (
    <ol>
      {Otps.map((otp) => (
        <li>
          <a
            href={`https://euw.op.gg/summoner/userName=${otp.name}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {otp.name}
          </a>
        </li>
      )) }

    </ol>
  );
}
