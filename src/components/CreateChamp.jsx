import React from 'react';
import { Link } from 'react-router-dom';

class CreateChamp extends React.PureComponent {
  constructor(props) {
    super(props);
    this.data = props.data;
    this.id = this.data.id;
    this.url = this.data.url;
    this.name = this.data.name;
    this.icon = this.data.icon;
    this.key = this.data.key;
  }

  render() {
    return (
      <div className="champion-icon champList" id={this.id}>
        <Link to={`${this.id}?id=${this.key}`}>
          <img src={this.icon} alt={this.name} />
          <span>{this.name}</span>
        </Link>
      </div>
    );
  }
} export default CreateChamp;
