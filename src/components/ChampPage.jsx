import React from 'react';
import champions from 'lol-champions';
import {
  useLocation,
} from 'react-router-dom';
import Chat from './Chat/Chat';
import Otp from './Otp';

export default function ChampPage() {
  const query = new URLSearchParams(useLocation().search);
  const MyChamp = champions.filter((champion) => champion.key === query.get('id'))[0];
  return (
    <div className="row align-items-center">
      <div className="col">
        <Otp id={MyChamp.key} />
      </div>
      <div className="col">
        <img
          className="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto"
          src={MyChamp.icon}
          width="250"
          height="250"
          xmlns={MyChamp.icon}
          aria-label="Espace réservé: 500 x 500"
          preserveAspectRatio="xMidYMid slice"
        />
        <div className="col-md-7">
          <h2 className="featurette-heading">
            <font>
              <font>{MyChamp.name}</font>
            </font>
            <span className="text-muted">
              <font>
                <font>{` ${MyChamp.title}`}</font>
              </font>
            </span>
          </h2>
          <p className="lead">
            <font>
              <font>
                {MyChamp.description}
              </font>
            </font>
          </p>
        </div>
      </div>
      <div className="col">
        <Chat RoomName={MyChamp.name} />
      </div>
    </div>
  );
}
