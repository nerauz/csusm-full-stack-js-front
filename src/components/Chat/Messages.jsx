import React from 'react';

import ScrollToBottom from 'react-scroll-to-bottom';

import Message from './Message';

import '../../css/Messages.css';

export default function Messages({ mes, name }) {
  return (
    <ScrollToBottom className="messages">
      {mes.map((msg) => <div key={Math.random()}><Message message={msg} username={name} /></div>)}
    </ScrollToBottom>
  );
}
