import React from 'react';

import ReactEmoji from 'react-emoji';

import '../../css/Message.css';

export default function Message({ message: { name, msg }, username }) {
  let isSentByCurrentUser = false;

  if (name === username) {
    isSentByCurrentUser = true;
  }

  return (
    isSentByCurrentUser ? (
      <div className="messageContainer justifyEnd">
        <p className="sentText pr-10">{username}</p>
        <div className="messageBox backgroundBlue">
          <p className="messageText colorWhite">{ReactEmoji.emojify(msg)}</p>
        </div>
      </div>
    )
      : (
        <div className="messageContainer justifyStart">
          <div className="messageBox backgroundLight">
            <p className="messageText colorDark">{msg}</p>
          </div>
          <p className="sentText pl-10">{name}</p>
        </div>
      )
  );
}
