import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';
import { useSelector } from 'react-redux';

import Input from './Input';
import Messages from './Messages';
import '../../css/Chat.css';

let socket;
const GetUserData = (state) => state;

export default function Chat(props) {
  const UserData = useSelector(GetUserData);
  // const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const ENDPOINT = 'http://127.0.0.1:3000';

  useEffect(() => {
    socket = io(ENDPOINT, { transports: ['websocket'] });

    socket.on('join', (data) => {
      setMessages((msgs) => [...msgs, data]);
    });

    socket.on('message', (data) => {
      setMessages((msgs) => [...msgs, data]);
    });

    socket.emit('join', { name: UserData.Data.username, room: props.RoomName }, (error) => {
      if (error) {
        alert(error);
      }
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();

    if (message) {
      socket.emit('send-to-room', { name: UserData.Data.username, message, room: props.RoomName }, () => setMessage(''));
      setMessages((msgs) => [...msgs, { msg: message, name: UserData.Data.username }]);
      setMessage('');
    }
  };

  return (
    <div className="outerContainer">
      <div className="container">
        <Messages mes={messages} name={UserData.Data.username} />
        <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
      </div>
    </div>
  );
}
// <InfoBar room={room} />
//  <TextContainer users={users} />
//   <Messages messages={messages} name={name} />
