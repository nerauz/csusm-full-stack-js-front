import React from 'react';
import 'font-awesome/css/font-awesome.min.css';
import '../../css/Input.css';

const Input = ({ message, setMessage, sendMessage }) => (
  <form className="form">
    <input
      className="input"
      type="text"
      placeholder="Type a message..."
      value={message}
      onChange={(e) => setMessage(e.target.value)}
      onKeyPress={(e) => { if (e.key === 'Enter') { return (sendMessage(e)); } return null; }}
    />
    <button type="button" className="sendButton" onClick={(e) => sendMessage(e)}>
      <div>
        <i className="fa fa-paper-plane" />
      </div>
    </button>
  </form>
);
export default Input;

//  onKeyPress={(e) => e.key === 'Enter' ? sendMessage(e) : null}
